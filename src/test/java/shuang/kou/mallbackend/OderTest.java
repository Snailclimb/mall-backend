package shuang.kou.mallbackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import shuang.kou.mallbackend.entity.Goods;
import shuang.kou.mallbackend.entity.Orders;
import shuang.kou.mallbackend.repository.GoodsRepository;
import shuang.kou.mallbackend.repository.OrdersRepositopry;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

public class OderTest extends IntegrationTestBase {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    GoodsRepository goodsRepository;

    @Autowired
    OrdersRepositopry ordersRepositopry;

    Goods goods = new Goods("good1", new BigDecimal(15), "瓶", "http://www.ruanyifeng.com/blogimg/asset/2017/bg2017122702.png");
    Goods good657s2 = new Goods("good1", new BigDecimal(15), "瓶", "http://www.ruanyifeng.com/blogimg/asset/2017/bg2017122702.png");

    @Test
    void should_add_order() throws Exception {
        Goods save = goodsRepository.save(goods);
        mockMvc.perform(post("/api/orders")
                .content(objectMapper.writeValueAsString(save))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().is(201));
        Optional<Orders> optionalOrders = ordersRepositopry.findById(1l);
        assertNotNull(optionalOrders.get());
    }

    @Test
    void should_get_order_response() throws Exception {
        Goods savedGoods = goodsRepository.save(goods);
        Orders orders = new Orders();
        orders.setGoods(savedGoods);
        orders.setGoodsNum(1);
        ordersRepositopry.save(orders);
        Optional<Orders> optionalOrders = ordersRepositopry.findById(1l);
        mockMvc.perform(get("/api/orders"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(content().string("[{\"name\":\"good1\",\"price\":15,\"goodsNum\":1,\"unit\":\"瓶\"}]"));
    }

}
