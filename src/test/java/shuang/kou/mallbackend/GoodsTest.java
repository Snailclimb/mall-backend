package shuang.kou.mallbackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import shuang.kou.mallbackend.entity.Goods;
import shuang.kou.mallbackend.repository.GoodsRepository;

import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GoodsTest extends IntegrationTestBase {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    GoodsRepository goodsRepository;

    Goods goods = new Goods("good1", new BigDecimal(15), "瓶", "http://www.ruanyifeng.com/blogimg/asset/2017/bg2017122702.png");
    Goods goods2 = new Goods("good1", new BigDecimal(15), "瓶", "http://www.ruanyifeng.com/blogimg/asset/2017/bg2017122702.png");

    @Test
    void should_add_goods() throws Exception {
        mockMvc.perform(post("/api/goods")
                .content(objectMapper.writeValueAsString(goods)).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(201));
        Optional<Goods> optionalGoods = goodsRepository.findById(1l);
        assertNotNull(optionalGoods.get());
        assertEquals("good1", optionalGoods.get().getName());
    }

    @Test
    void should_return_400_if_request_is_not_valid() throws Exception {
        Goods goods = new Goods();
        mockMvc.perform(post("/api/goods")
                .content(objectMapper.writeValueAsString(goods)).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.name").value("商品名字不能为空"))
                .andExpect(jsonPath("$.price").value("价格不能为空"))
                .andExpect(jsonPath("$.imageUrl").value("图片地址不能为空"));
    }

    @Test
    void should_return_400_if_goods_name_is_already_exists() throws Exception {
        goodsRepository.save(goods);// 数据库中已经存在名称为good1的商品
        mockMvc.perform(post("/api/goods")
                .content(objectMapper.writeValueAsString(goods)).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(400));
    }


    @Test
    void should_get_all_goods() throws Exception {
        goodsRepository.save(goods);
        goodsRepository.save(goods2);
        mockMvc.perform(get("/api/goods").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", hasSize(2)));
    }

}
