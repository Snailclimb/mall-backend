package shuang.kou.mallbackend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author shuang.kou
 */
@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    // private long userId;

    @Column(name = "num")
    @NotNull(message = "商品数量不能为空")
    private int goodsNum;

    @ManyToOne
    @JoinColumn(name = "Goods")
    Goods goods;

    public long getId() {
        return id;
    }

    public int getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(int goodsNum) {
        this.goodsNum = goodsNum;
    }

    public Orders() {
    }

    public Orders(int goodsNum, Goods goods) {
        this.goodsNum = goodsNum;
        this.goods = goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }
}
