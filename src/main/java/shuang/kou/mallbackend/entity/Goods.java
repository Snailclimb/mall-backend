package shuang.kou.mallbackend.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author shuang.kou
 */
@Entity
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    @NotNull(message = "商品名字不能为空")
    private String name;

    @Column(name = "price")
    @NotNull(message = "价格不能为空")
    private BigDecimal price;


    @Column(name = "unit")
    @NotNull(message = "单位不能为空")
    private String unit;

    @Column(name = "imageUrl")
    @NotNull(message = "图片地址不能为空")
    private String imageUrl;

    public Goods() {
    }

    public Goods(@NotNull(message = "商品名字不能为空") String name, @NotNull(message = "价格不能为空") BigDecimal price, @NotNull(message = "单位不能为空") String unit, @NotNull(message = "图片地址不能为空") String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
