package shuang.kou.mallbackend.contract;

import shuang.kou.mallbackend.entity.Goods;
import shuang.kou.mallbackend.entity.Orders;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author shuang.kou
 */
public class CreateOrderResponse {

    private String name;
    private BigDecimal price;
    private int goodsNum;
    private String unit;

    public CreateOrderResponse() {
    }

    public CreateOrderResponse(Goods goods, Orders orders) {
        // 下面这两句代码很重要，不然会报错
        Objects.requireNonNull(goods);
        Objects.requireNonNull(orders);
        this.name = goods.getName();
        this.price = goods.getPrice();
        this.goodsNum = orders.getGoodsNum();
        this.unit = goods.getUnit();
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getGoodsNum() {
        return goodsNum;
    }

    public String getUnit() {
        return unit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setGoodsNum(int goodsNum) {
        this.goodsNum = goodsNum;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
