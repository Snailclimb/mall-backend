package shuang.kou.mallbackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shuang.kou.mallbackend.contract.CreateOrderResponse;
import shuang.kou.mallbackend.entity.Goods;
import shuang.kou.mallbackend.entity.Orders;
import shuang.kou.mallbackend.repository.GoodsRepository;
import shuang.kou.mallbackend.repository.OrdersRepositopry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrdersService {
    private final GoodsRepository goodsRepository;
    private final OrdersRepositopry ordersRepositopry;

    public OrdersService(OrdersRepositopry ordersRepositopry, GoodsRepository goodsRepository) {
        this.ordersRepositopry = ordersRepositopry;
        this.goodsRepository = goodsRepository;
    }

    public void addOrder(Goods goods) {
        Optional<Orders> optionalOrders = ordersRepositopry.findByGoods_Id(goods.getId());
        if (optionalOrders.isPresent()) {// 订单已经有该商品，商品数量加1
            Orders orders = optionalOrders.get();
            orders.setGoodsNum(orders.getGoodsNum() + 1);
            ordersRepositopry.save(orders);
        } else {
            Orders orders = new Orders();
            orders.setGoods(goods);
            orders.setGoodsNum(1);
            ordersRepositopry.save(orders);
        }
    }

    public List<CreateOrderResponse> getAllOrders() {
        List<Orders> allOrders = ordersRepositopry.findAll();
        List<CreateOrderResponse> createOrderResponseList = new ArrayList<>();
        allOrders.forEach(orders -> {
            long id = orders.getId();
            Goods goods = goodsRepository.findById(id).get();
            createOrderResponseList.add(new CreateOrderResponse(goods, orders));
        });
        return createOrderResponseList;
    }
}
