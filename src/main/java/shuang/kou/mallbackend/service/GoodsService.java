package shuang.kou.mallbackend.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shuang.kou.mallbackend.entity.Goods;
import shuang.kou.mallbackend.exception.GoodsAlreadyExistException;
import shuang.kou.mallbackend.repository.GoodsRepository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @author shuang.kou
 */
@Service
public class GoodsService {

    private final GoodsRepository goodsRepository;

    public GoodsService(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    public void addGoods(Goods goods) {
        Optional<Goods> optionalGoods = goodsRepository.findByName(goods.getName());
        if (optionalGoods.isPresent()) {
            throw new GoodsAlreadyExistException("商品名称已存在，请您输入新的商品名称！");
        }
        goodsRepository.save(goods);
    }

    public Page<Goods> findAllGoods(Pageable pageable) {
        return goodsRepository.findAll(pageable);
    }

    public Goods getGoodsByID(long id) {
        return goodsRepository.findById(id).get();
    }
}
