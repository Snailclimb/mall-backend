package shuang.kou.mallbackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author shuang.kou
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class GoodsAlreadyExistException extends RuntimeException {

    public GoodsAlreadyExistException() {
    }

    public GoodsAlreadyExistException(String message) {
        super(message);
    }
}
