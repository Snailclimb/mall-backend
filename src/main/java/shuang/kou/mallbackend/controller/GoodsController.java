package shuang.kou.mallbackend.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shuang.kou.mallbackend.entity.Goods;
import shuang.kou.mallbackend.service.GoodsService;

import javax.validation.Valid;


/**
 * @author shuang.kou
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class GoodsController {

    private final GoodsService goodsService;

    public GoodsController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @PostMapping("/goods")
    public ResponseEntity<Goods> addGoods(@RequestBody @Valid Goods goods) {
        goodsService.addGoods(goods);
        return ResponseEntity.status(201).build();
    }

    @GetMapping("/goods")
    public ResponseEntity<Page<Goods>> getGoods(Pageable pageable) {
        Page<Goods> all = goodsService.findAllGoods(pageable);
        return ResponseEntity.status(200).body(all);
    }
}
