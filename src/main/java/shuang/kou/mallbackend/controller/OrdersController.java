package shuang.kou.mallbackend.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shuang.kou.mallbackend.contract.CreateOrderResponse;
import shuang.kou.mallbackend.entity.Goods;
import shuang.kou.mallbackend.entity.Orders;
import shuang.kou.mallbackend.service.GoodsService;
import shuang.kou.mallbackend.service.OrdersService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class OrdersController {

    private final OrdersService ordersService;

    private final GoodsService goodsService;

    public OrdersController(OrdersService ordersService, GoodsService goodsService) {
        this.ordersService = ordersService;
        this.goodsService = goodsService;
    }

    @PostMapping("/orders")
    public ResponseEntity addOrder(@RequestBody Goods goods) {
        ordersService.addOrder(goods);
        return ResponseEntity.status(201).build();
    }


    @GetMapping("/orders")
    public ResponseEntity<List<CreateOrderResponse>> getAllOrders() {
        List<CreateOrderResponse> createOrderResponseList = ordersService.getAllOrders();
        return ResponseEntity.status(200).body(createOrderResponseList);
    }
}
