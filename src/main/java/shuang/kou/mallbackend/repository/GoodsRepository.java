package shuang.kou.mallbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import shuang.kou.mallbackend.entity.Goods;

import java.util.Optional;


/**
 * @author shuang.kou
 */
@Repository
public interface GoodsRepository extends JpaRepository<Goods, Long> {

   Optional<Goods> findByName(String name);
}
