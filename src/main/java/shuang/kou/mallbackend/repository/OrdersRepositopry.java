package shuang.kou.mallbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import shuang.kou.mallbackend.entity.Orders;

import java.util.Optional;

@Repository
public interface OrdersRepositopry extends JpaRepository<Orders, Long> {
    Optional<Orders> findByGoods_Id(Long id);
}
